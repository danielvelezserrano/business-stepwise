/**************************************************************************************************/
/* This function implements business stepwise method for a binary target variable.                */
/* The idea of the algorithm is based on classical stepwise regression method, allowing the       */
/* possibility of control the signs of the parameters estimated according to the business knowing */
/* of the analyst about the problem he is dealing with.                                           */
/* The parameters associated to the function are:                                                 */
/*                                                                                                */
/* inputTable.- table with data to be modelled with a logistic regression model.                  */
/*                                                                                                */
/* metadataTable.- table with four columns:                                                       */
/* 1. varName.- its values are names of variables contained in the table referenced in inputTable.*/
/* 2. rol.- rol of each one of the variables identified by varName (TARGET, INPUT or ID).         */
/* 3. measure.- type of measure associated to variables contained in the table referenced in      */
/* input variable (INTERVAL for continuous variables, ORDINAL, NOMINAL, BINARY (for discrete ones)*/
/* 4. expectedSign.- expected sign for the parameter associated to input variables referenced     */
/* in inputTable ("+","-" or "" (if there is no knowing about the expected sign of the parameter  */
/*                                                                                                */
/* scoringCode.- name of the file where logistic regression scoring code will be written.         */
/* Name must be preceeded of the directory where file will be stored.                             */
/*                                                                                                */
/* Example: %businessStepwise(inputTable=work.trainingTable,                                      */ 
/*                            metadataTable=work.metadata,                                        */
/*                            scoringCode=c:\scoringRegressionCode.sas                            */
/**************************************************************************************************/

%macro businessStepwise
			(
        inputTable=, 
        metadataTable=,
        scoringCode=
			);

  /*****************************************************/
  /* Target and input variables metadata are generated */
  /*****************************************************/

  data work.targetMetadataTable work.inputsMetadataTable;
    set &metadataTable;
    if compress(upcase(rol))="TARGET" then output work.targetMetadataTable;
    else if compress(upcase(rol))="INPUT" and compress(upcase(measure)) in ("INTERVAL","BINARY","ORDINAL","NOMINAL") 
    then output work.inputsMetadataTable;
  run;

  data _null_;
    set work.targetMetadataTable;
    call symput('targetVariable',compress(varName));
  run;

  proc sql;
    drop table work.targetMetadataTable;
  quit;

  /* Scoring code is initialized */

  filename code "&scoringCode";
  
  data _null_;
    file code;
    put "/* Scoring code for predicting target variable = &targetVariable */";
  run;

  data work.classMetadataTable;
    set work.inputsMetadataTable;
    if upcase(compress(measure)) in ("NOMINAL","ORDINAL","BINARY");
  run;

  proc sql;
    drop table work.inputsMetadataTable;
  quit;

  %let numberClassInputs=0;

  data _null_;
    set work.classMetadataTable;
    call symput('classInputVariable_'||left(_n_),compress(varName));
    call symput('numberClassInputs',_n_);
  run;

  proc sql;
    drop table work.classMetadataTable;
  quit;

  /************************************************************/
  /* List with variables selected by the model is initialized */
  /************************************************************/

  data work.selectedVariables;
    length varName $32.;
    stop;
  run;

  /***********************************************************************************************************/
  /* This table controls if the number of input variables selected by the model increases after an iteration */
  /* It means, this table controls if a new variable has been included in the model                          */
  /***********************************************************************************************************/

  data work.estimatedParametersCopyT;
    length varName $32.;
    stop;
  run;

  data work.possibleVariables;
    set &metadataTable;
    where upcase(compress(rol))="INPUT" and compress(upcase(measure)) in ("INTERVAL","BINARY","ORDINAL","NOMINAL");
  run;

  %let stopCriteria=1;

  %do %while (&stopCriteria>0);
    
    %let numberSelectedVariables=0;

    data _null_;
      set work.selectedVariables;
      call symput('selectedVariable_'||left(_n_),compress(varName));
      call symput('numberSelectedVariables',_n_);
    run;

    %let numberPossibleVariables=0;

    data _null_;
      set work.possibleVariables;
      call symput('possibleVariable_'||left(_n_),varName);
      call symput('numberPossibleVariables',_n_);
    run;

    data work.estimatedParameters;
      length varName $32.;
      stop;
    run;

    proc logistic data=&inputTable covout outest=work.estimatedParameters descending noprint;
      %if &numberClassInputs>0 %then %do;
        class 
        %do countClassInputVariables=1 %to &numberClassInputs;
          &&classInputVariable_&countClassInputVariables  
        %end;
        ;
      %end;
      model &targetVariable =
      %if &numberSelectedVariables>0 %then %do;
        %do countSelectedVariables=1 %to &numberSelectedVariables;
          &&selectedVariable_&countSelectedVariables
        %end;
      %end;
      %if &numberPossibleVariables>0 %then %do;
        %do countPossibleVariables=1 %to &numberPossibleVariables;
          &&possibleVariable_&countPossibleVariables
        %end;
      %end;
      /
      link=logit
      selection=stepwise maxstep=1 include=&numberSelectedVariables;
    run;
    quit;

    data work.estimatedParametersI work.estimatedParametersII;
      set work.estimatedParameters;
      if upcase(compress(_TYPE_))="PARMS" then output work.estimatedParametersI;
      else output work.estimatedParametersII;
    run;

    proc transpose data=work.estimatedParametersI(drop=_NAME_)
      out=work.estimatedParametersT(rename=(_name_=varName col1=estimate))
      ;
    run;
    quit;

    proc sql;
      drop table work.estimatedParametersI;
    quit;

    data _null_;
      set work.estimatedParametersII;
      call symput('estimatedParameterName'||left(_n_),upcase(compress(_NAME_)));
      call symput('numberEstimatedParameters',_n_);
    run;

    %do countEstimatedParameters=1 %to &numberEstimatedParameters;

      data _null_;
        set work.estimatedParametersII;
        if upcase(compress(_NAME_))=upcase(compress("&&estimatedParameterName&countEstimatedParameters")) then
        call symput('std',sqrt(&&estimatedParameterName&countEstimatedParameters));
      run;

      data work.estimatedParametersT;
        set work.estimatedParametersT;
        if upcase(compress(varName))=upcase(compress("&&estimatedParameterName&countEstimatedParameters")) then
        tvalue=estimate/&std;
      run;

    %end;

    proc sql;
      drop table work.estimatedParametersII;
    quit;

    /***************************************************************************************/
    /* If a class variable is selected, its name is sufixed by a number or word associated */
    /* to the class value. In _LABEL_ variable, this sufix appears after a blank and so    */
    /* it's posibble to build the name of the variable                                     */
    /***************************************************************************************/

    data work.estimatedParametersT;
      set work.estimatedParametersT;
      if compress(_LABEL_) ne "" and upcase(varName) ne "_STEP_" and
      index(upcase(varName),"INTERCEPT")=0 then varName=scan(_LABEL_,1,"");
    run;

    proc sort data=work.estimatedParametersT nodupkey;
      by varName;
    run;
    quit;

    %let unexpectedSign=0;

    proc sql;
      create table work.estimatedParametersT as
      select a.*,
      b.expectedSign,
      b.measure
      from work.estimatedParametersT a inner join &metadataTable b
      on a.varName=b.varName
      where estimate ne .;
    quit;

    proc sort data=work.estimatedParametersT;
      by varName;
    run;
    quit;

    data work.unexpectedSignParameters;
      set work.estimatedParametersT;
      where (.<abs(tvalue)<1.96 or (estimate>0 and expectedSign="-") or (estimate<0 and expectedSign="+"))
      and index(upcase(varName),"INTERCEPT")=0;
    run;

    /******************************************************************************************************/
    /* 1 value is only read if table unexpectedSignParameters exist (that is, if there are negative parameters */
    /* If a variable X entries in the model and some of the variables previously selected change its sign */
    /* (distinct to the sign defined in the metadata), then X is not included in the model                */
    /******************************************************************************************************/

    data _null_;
      set work.unexpectedSignParameters;
      call symput('unexpectedSign',1); 
    run;

    data work.newVariable;
      merge work.estimatedParametersT(in=a) work.estimatedParametersCopyT(in=b);
      by varName;
      if a and not b;
    run;

    %let selectedVariable=;

    %let variableForModelling=0;

    data _null_;
      set work.newVariable;
      call symput('selectedVariable',compress(varName));
      if (estimate>0 and expectedSign="+") or (estimate<0 and expectedSign="-") or (expectedSign not in ("+","-")) 
      or upcase(measure)="NOMINAL" then do;
        call symput('variableForModelling',1);
      end;
    run;

    %if &unexpectedSign=1 %then %do;

      %let variableForModelling=0;

    %end;

    /*******************************************************************************************/
    /* If a variable has been selected, it will not be a candidate, it will entry in the model */ 
    /*******************************************************************************************/

    %if "&selectedVariable" ne "" %then %do;

      %if &variableForModelling=1 %then %do;

        data work.possibleVariables;
          set work.possibleVariables;
          where upcase(compress(varName)) ne upcase(compress("&selectedVariable"));
        run;

        %let stopCriteria=0;

        data _null_;
          set work.possibleVariables;
          call symput('stopCriteria',_n_);
        run;

        %if &numberSelectedVariables=0 %then %do;;

          data work.selectedVariables;
              length varName $32.;
              varName="&selectedVariable";
          run;

        %end;
        %else %do;

          data work.selectedVariables;
            set work.selectedVariables end=last;
            output;
            if last then do;
              varName="&selectedVariable";
              output;
            end;
          run;

        %end;

      %end;
      %else %do;

        data work.possibleVariables;
          set work.possibleVariables;
          where upcase(compress(varName)) ne upcase(compress("&selectedVariable"));
        run;

        %let stopCriteria=0;

        data _null_;
          set work.possibleVariables;
          call symput('stopCriteria',_n_);
        run;

      %end;

    %end;
    %else %do; /* Variables don't entry in the model */

      %let stopCriteria=0;

    %end;

    /******************************************************************************/
    /* A copy of the last parameters estimation is generated in order to check if */
    /* a new variables has entried in the next iteration                          */
    /******************************************************************************/

    data work.estimatedParametersCopyT;
      set estimatedParametersT;
    run;

    proc sql;
      drop table work.newVariable;
    quit;

  %end; /* End of "do while" bucle */

  %let numberSelectedVariables=0;

  data _null_;
    set work.selectedVariables;
    call symput('selectedVariable_'||left(_n_),compress(varName));
    call symput('numberSelectedVariables',_n_);
  run;

  /***************************************************************************************************/
  /* Estimated parameters are showed in the order in which variables have been included in the model */
  /***************************************************************************************************/

  proc logistic data=&inputTable 
                outest=work.estimatedParameters(drop=_name_ where=(_TYPE_="PARMS")) descending;
    %if &numberClassInputs>0 %then %do;
      class 
      %do countClassInputVariables=1 %to &numberClassInputs;
        &&classInputVariable_&countClassInputVariables  
      %end;
      ;
    %end;
    model &targetVariable =
    %if &numberSelectedVariables>0 %then %do;
      %do countSelectedVariables=1 %to &numberSelectedVariables;
        &&selectedVariable_&countSelectedVariables
      %end;
    %end;
    /
    link=logit
    ;
  run;
  quit;

  proc sql;
    drop table work.estimatedParametersCopyT,work.unexpectedSignParameters,
               work.possibleVariables,work.selectedVariables;
  quit;

  /***************************/
  /* Scoring code generation */
  /***************************/

  proc transpose data=work.estimatedParameters
    out=work.estimatedParametersT(rename=(_name_=varName col1=estimate));
  run;
  quit;

  data work.intercept;
    set work.estimatedParametersT;
    if index(upcase(varName),"INTERCEPT")>0;
  run;

  data work.estimatedParametersT;
    set work.estimatedParametersT;
    variableCopy=varName;
    if compress(_LABEL_) ne "" and upcase(varName) ne "_STEP_" and
    index(upcase(varName),"INTERCEPT")=0 then varName=scan(_LABEL_,1,"");
  run;

  proc sql;
    create table work.estimatedParametersT as
    select a.*,
    b.expectedSign,
    b.measure
    from work.estimatedParametersT a inner join &metadataTable b
    on a.varName=b.varName
    where estimate ne .;
  quit;

  data work.finalEstimatedParameters(drop=variableCopy);
    set work.estimatedParametersT;
    varName=variableCopy;
    if _label_ ne "" then do;
      variablePrefix=compress(scan(_label_,1,""));
      variableSufix=compress(scan(_label_,2,""));
    end;
    else do;
      variablePrefix=varName;
    end;
  run;

  proc sort data=work.finalEstimatedParameters;
    by variablePrefix variableSufix;
  run;
  quit;

  data _null_;
    set work.finalEstimatedParameters;
    call symput('variableModelo'||left(_n_),compress(variablePrefix));
    call symput('claseVariableModelo'||left(_n_),compress(variableSufix));
    call symput('numberModelVariables',compress(_n_));
  run;

  %do countModelVariables=1 %to &numberModelVariables;

    %if "&&claseVariableModelo&countModelVariables" ne "" %then %do;

      proc sql;
        create table work.classVariables as
        select distinct &&variableModelo&countModelVariables as previousVariableSufix
        from &inputTable;
      quit;

      data work.classVariables(drop=previousVariableSufix);
        length variablePrefix $32.;
        set work.classVariables;
        variableSufix=compress(previousVariableSufix);
        variablePrefix="&&variableModelo&countModelVariables";
      run;

      proc sort data=work.classVariables;
        by variablePrefix variableSufix;
      run;
      quit;
   
      data work.finalEstimatedParameters;
        update work.finalEstimatedParameters work.classVariables;
        by variablePrefix variableSufix;
      run;

      proc sql;
        drop table work.classVariables;
      quit;

    %end;

  %end;

  data work.finalEstimatedParameters(drop=acumulatedEstimation);
    set work.finalEstimatedParameters;
    retain acumulatedEstimation;
    by variablePrefix variableSufix;
    if first.variablePrefix then acumulatedEstimation=0;
    acumulatedEstimation=sum(acumulatedEstimation,estimate);
    if last.variablePrefix=1 and estimate=. then estimate=-acumulatedEstimation;
  run;

  data work.finalEstimatedParameters;
    set work.intercept work.finalEstimatedParameters;
  run;

  proc sql;
    drop table work.intercept;
  quit;

	data work.scoringParametersTable(keep=varName classval0 estimate);
		set work.finalEstimatedParameters;
    if index(upcase(varName),"INTERCEPT") eq 0 then do;
      varName=variablePrefix;
      classval0=variableSufix;
    end;
  run;

  data _null_;
    dsid=open('work.scoringParametersTable');
    check=varnum(dsid,'classval0');
    call symput('existe_class',check);
    rc=close(dsid);
  run;

  %if &existe_class=0 %then %do;
    data work.scoringParametersTable;
    	set work.scoringParametersTable;
    	classval0="";
    run;
  %end;

  proc sql noprint;
    select count(*) into:n_obs_num from work.scoringParametersTable where missing(classval0);
    select count(*) into:n_obs_char from work.scoringParametersTable where not missing(classval0);
    %if %symexist(n_obs_char) %then %do;
    	%if &n_obs_char>0 %then %do;
    		select put(estimate,comma32.12) into:beta_char1-:beta_char%left(&n_obs_char) from work.scoringParametersTable where not missing(classval0);
    		select "(compress("||varName||")='"||compress(classval0)||"')" into:comp1-:comp%left(&n_obs_char) from work.scoringParametersTable where not missing(classval0);
    	%end;
    %end;
    %if %symexist(n_obs_num) %then %do;
    	%if &n_obs_num>0 %then %do;
    		select varName into:v1-:v%left(&n_obs_num) from work.scoringParametersTable where missing(classval0);
    		select put(estimate,comma32.12) into:beta_num1-:beta_num%left(&n_obs_num) from work.scoringParametersTable where missing(classval0);
    	%end;
    %end;
    select count(distinct(varName)) into:n_var from work.scoringParametersTable(where=(compress(upcase(varName)) not in ("INTERCEPT")));
    select distinct varName into:w1-:w%left(%eval(&n_var)) from work.scoringParametersTable(where=(compress(upcase(varName)) not in ("INTERCEPT")));
  quit;

  proc sql;
    create table work.targetValues as
    select distinct &targetVariable
    from &inputTable;
  run;
  quit;

  proc sort data=work.targetValues;
    by descending &targetVariable;
  run;
  quit;

  data _null_;
    set work.targetValues;
    if _n_=1 then do;
      call symput('targetEvent',compress(&targetVariable));
    end;
    if _n_=2 then do;
      call symput('targetNotEvent',compress(&targetVariable));
    end;
  run;

  proc sql;
    drop table work.targetValues;
  quit;

  data _null_;
    file CODIGO MOD;
  	put "x_beta=0";
  	%if %symexist(n_obs_num) %then %do;
  		%if &n_obs_num>0 %then %do;
  			%do i=1 %to &n_obs_num;
          %if %upcase("&&v&i") ne "INTERCEPT" %then %do;
  				  put "+ &&v&i *( &&beta_num&i)";
          %end;
          %else %do;
  				  put "+ ( &&beta_num&i)";
          %end;
  			%end;
  		%end;
  	%end;
  	%if %symexist(n_obs_char) %then %do;
  		%if &n_obs_char>0 %then %do;
  			%do i=1 %to &n_obs_char;
          %if %upcase("&&comp&i") ne "INTERCEPT" %then %do;
    				put "+ &&comp&i *( &&beta_char&i)";
          %end;
          %else %do;
  				  put "+ ( &&beta_char&i)";
          %end;
  			%end;
  		%end;
  	%end;
  	;
    put ";";
  	put "P_&targetVariable.&targetEvent=1/(1+exp(-x_beta));";
  	put "P_&targetVariable.&targetNotEvent=1-P_&targetVariable.&targetEvent;";
  run;

  proc sql;
    drop table work.estimatedParameters,work.estimatedParametersT,work.finalEstimatedParameters;
  quit;

%mend businessStepwise;

ods listing;

libname dvs "C:\Proyectos\UNIVERSIDAD\data";

%businessStepwise(
inputTable=dvs.churnTrainingTable,
metadataTable=dvs.churnMetadata,
scoringCode=C:\Proyectos\UNIVERSIDAD\churnScoringCode.sas
);

data work.testTable;
  set dvs.Churntesttable;
  %include "C:\Proyectos\UNIVERSIDAD\churnScoringCode.sas";
run;

proc logistic data=dvs.churnTrainingTable descending;
  model target=W_:/selection=stepwise;
run;
quit;

data work.metadata2;
  set dvs.churnMetadata;
  if varName="W_NUM_JOINED_ACCOUNT" then expectedSign="+";
run;

%businessStepwise(
inputTable=dvs.churnTrainingTable,
metadataTable=work.metadata2,
scoringCode=C:\Proyectos\UNIVERSIDAD\churnScoringCode2.sas);

data work.testTable2;
  set dvs.Churntesttable;
  %include "C:\Proyectos\UNIVERSIDAD\churnScoringCode2.sas";
run;
