 This function implements business stepwise method for a binary target variable.                
 The idea of the algorithm is based on classical stepwise regression method, allowing the       
 possibility of control the signs of the parameters estimated according to the business knowing 
 of the analyst about the problem he is dealing with.                                           
 The parameters associated to the function are:                                                 
                                                                                                
 inputTable.- table with data to be modelled with a logistic regression model.                  
                                                                                                
 metadataTable.- table with four columns:                                                       
 1. varName.- its values are names of variables contained in the table referenced in inputTable.
 2. rol.- rol of each one of the variables identified by varName (TARGET, INPUT or ID).         
 3. measure.- type of measure associated to variables contained in the table referenced in      
 input variable (INTERVAL for continuous variables, ORDINAL, NOMINAL, BINARY (for discrete ones)
 4. expectedSign.- expected sign for the parameter associated to input variables referenced     
 in inputTable ("+","-" or "" (if there is no knowing about the expected sign of the parameter  
                                                                                                
 scoringCode.- name of the file where logistic regression scoring code will be written.         
 Name must be preceeded of the directory where file will be stored.                             
                                                                                                
 Example: %businessStepwise(inputTable=work.trainingTable,                                       
                            metadataTable=work.metadata,
                            scoringCode=c:\scoringRegressionCode.sas);                            
